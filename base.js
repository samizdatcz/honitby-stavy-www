var map;

var table = "https://datazurnal.cz/arcgis/rest/services/honitby/MapServer/1/";

var zverNazvy = {
        'JELENISA': 'jelení zvěř',
        'DANCISA': 'cekem dančí zvěř',
        'MUFLONISA': 'cekem mufloní zvěř',
        'SRNCISA': 'srnčí zvěř',
        'CERNASA': 'černá zvěř',
        'SIKAJAPDYBSA': 'zvěř sika',
        'JELENCESA': 'jelencová zvěř',
        'KAMZICISA': 'kamzičí zvěř',
        'ZAJIC': 'zajíc polní',
        'LISKA': 'liška obecná',
        'JEZEVEC': 'jezevec lesní',
        'KUNALESNISKALNI': 'kuna skalní a lesní',
        'ONDATRA': 'ondatra pižmová',
        'HRIVNAC': 'holub hřivnáč',
        'STRAKA': 'straka obecná',
        'VRANA': 'vrána obená',
        'TCHORTMAVYSTEPNI': 'tchoř tmavý  stepní',
        'HRDLICKA': 'hrdlička zahradní',
        'SPACEK': 'špaček obecný',
        'KOCKA': 'kočka divoká',
        'LOS': 'los evropský',
        'VYDRA': 'vydra říční',
        'BOBR': 'bobr evropský',
        'VOLAVKA': 'volavka popelavá',
        'KORMORAN': 'kormorán velký',
        'JERABEK': 'jeřábek obecný',
        'TETREV': 'tetřev hlušec'
};

var breaks = {'BOBR_STAV_pct': [0.0,
			  0.0028951939779965257,
			  0.010775862068965518,
			  0.024067388688327317,
			  0.04836033478892567,
			  0.1772211720226843],
			 'HRDLICKA_STAV_pct': [0.0,
			  0.01710863986313088,
			  0.05337617661109317,
			  0.12285012285012285,
			  0.2907864319050873,
			  1.300578034682081],
			 'HRIVNAC_STAV_pct': [0.0,
			  0.03286384976525822,
			  0.1094889112367922,
			  0.33660589060308554,
			  0.8965152452417453,
			  2.28306779080576],
			 'JERABEK_STAV_pct': [0.0,
			  0.0012804097311139564,
			  0.0040941658137154556,
			  0.007380073800738007,
			  0.011591148577449948,
			  0.06493506493506493],
			 'JEZEVEC_STAV_pct': [0.0,
			  0.0025220680958385876,
			  0.006764984440535787,
			  0.014233647096291513,
			  0.04560697518443997,
			  0.14305177111716622],
			 'JKS_CERNASA_pct': [0.0,
			  0.018407960199004977,
			  0.12679425837320574,
			  0.4032258064516129,
			  0.8888888888888888,
			  6.67588574698393],
			 'JKS_DANCISA_pct': [0.0,
			  0.30911429686595193,
			  1.0714285714285714,
			  2.3,
			  4.0,
			  11.6],
			 'JKS_JELENCESA_pct': [0.0,
			  0.007115948095437421,
			  0.033623910336239106,
			  0.5098039215686274,
			  1.1918951132300357,
			  2.0],
			 'JKS_JELENISA_pct': [0.0,
			  0.14953271028037382,
			  0.5555555555555556,
			  1.14153389540288,
			  2.125,
			  5.5],
			 'JKS_KAMZICISA_pct': [0.0,
			  0.0015022958836842404,
			  0.005636978579481398,
			  0.00890956353106272,
			  0.01508319513355792,
			  0.034482758620689655],
			 'JKS_MUFLONISA_pct': [0.0,
			  0.21543408360128619,
			  0.7291666666666666,
			  1.5,
			  2.4,
			  7.836990595611285],
			 'JKS_SIKAJAPDYBSA_pct': [0.0,
			  0.016713091922005572,
			  0.06656580937972768,
			  0.14901960784313725,
			  0.33248891703609884,
			  0.9],
			 'JKS_SRNCISA_pct': [0.0,
			  0.02585410895660203,
			  0.04594400574300072,
			  0.06961613532856213,
			  0.15984015984015984,
			  0.475153079978125],
			 'JKS_ZAJIC_pct': [0.0,
			  0.023076923076923078,
			  0.05573770491803279,
			  0.10627177700348432,
			  0.19461077844311378,
			  1.4327272727272726],
			 'KOCKA_STAV_pct': [0.0,
			  0.0,
			  0.0,
			  0.0006000808548943885,
			  0.0,
			  0.0006000808548943885],
			 'KORMORAN_STAV_pct': [0.0,
			  0.023600809170600135,
			  0.08654262224145391,
			  0.19946808510638298,
			  0.38910505836575876,
			  0.8064516129032258],
			 'KUNALESNISKALNI_STAV_pct': [0.0,
			  0.012094823415578132,
			  0.031552851025467656,
			  0.09308636403917372,
			  0.1938576212700582,
			  0.6523050830873599],
			 'LISKA_STAV_pct': [0.0,
			  0.006990317187389957,
			  0.015551208492596806,
			  0.030902348578491966,
			  0.09433962264150944,
			  0.9784576246310399],
			 'LOS_STAV_pct': [0.0,
			  0.0,
			  0.0005945303210463733,
			  0.0007513148009015778,
			  0.0011574074074074073,
			  0.003389830508474576],
			 'ONDATRA_STAV_pct': [0.0,
			  0.006150200315099363,
			  0.021574973031283712,
			  0.05249343832020997,
			  0.1272264631043257,
			  0.35860609809669813],
			 'SPACEK_STAV_pct': [0.0,
			  0.12626262626262627,
			  0.539562619749179,
			  1.6501650165016502,
			  3.912984460247424,
			  21.020479187360582],
			 'STRAKA_STAV_pct': [0.0,
			  0.012605042016806723,
			  0.03676470588235294,
			  0.09309309309309309,
			  0.2975206611570248,
			  0.9193548387096774],
			 'TCHORTMAVYSTEPNI_STAV_pct': [0.0,
			  0.001689760054072322,
			  0.00581732677691519,
			  0.014013484335166672,
			  0.03474232773595831,
			  0.12],
			 'TETREV_STAV_pct': [0.0,
			  0.00035236081747709656,
			  0.0013491635186184566,
			  0.002593809441466367,
			  0.004246074849412462,
			  0.009687575684185032],
			 'VOLAVKA_STAV_pct': [0.0,
			  0.005053057099545225,
			  0.017049139096901648,
			  0.042796005706134094,
			  0.1111111111111111,
			  0.36855036855036855],
			 'VRANA_STAV_pct': [0.0,
			  0.0067921430187594745,
			  0.024968789013732832,
			  0.06472491909385113,
			  0.15754233950374164,
			  0.4803073967339097],
			 'VYDRA_STAV_pct': [0.0,
			  0.0014378145219266715,
			  0.0050335570469798654,
			  0.011834319526627219,
			  0.024903281879002426,
			  0.111358574610245]
};


var style = function (zvire, breaks) {
  var style = {}
  style.version = 8;
  style.sources = {}
  style.sources.honitby = {
      "type": "vector",
      "tiles": [location.origin+location.pathname+"./tiles/{z}/{x}/{y}.pbf"],
      "maxzoom": 12
      
    };
    style.layers = [
    {
      "id": "hon1",
      "interactive": true,
      "type": "fill",
      "source": "honitby",
      "source-layer": "honitby_stavy13",
      "filter": [
        "all",
        [">=", zvire, breaks[zvire][0]],
        ["<", zvire, breaks[zvire][1]]
        ],
      "paint": {
        "fill-color": "#2c7bb6",
        "fill-outline-color": "#d9d9d9",
        "fill-opacity": 0.8
    }
    },
    {
      "id": "hon2",
      "type": "fill",
      "interactive": true,
      "source": "honitby",
      "source-layer": "honitby_stavy13",
      "filter": [
        "all",
        [">=", zvire, breaks[zvire][1]],
        ["<", zvire, breaks[zvire][2]]
        ],
      "paint": {
        "fill-color": "#abd9e9",
        "fill-outline-color": "#d9d9d9",
        "fill-opacity": 0.8
    }
    },
    {
      "id": "hon3",
      "type": "fill",
      "interactive": true,
      "source": "honitby",
      "source-layer": "honitby_stavy13",
      "filter": [
        "all",
        [">=", zvire, breaks[zvire][2]],
        ["<", zvire, breaks[zvire][3]]
        ],
      "paint": {
        "fill-color": "#ffffbf",
        "fill-outline-color": "#d9d9d9",
        "fill-opacity": 0.8
    }
    },
    {
      "id": "hon4",
      "type": "fill",
      "interactive": true,
      "source": "honitby",
      "source-layer": "honitby_stavy13",
      "filter": [
        "all",
        [">=", zvire, breaks[zvire][3]],
        ["<", zvire, breaks[zvire][4]]
        ],
      "paint": {
        "fill-color": "#fdae61",
        "fill-outline-color": "#d9d9d9",
        "fill-opacity": 0.8
    }
    },{
      "id": "hon5",
      "type": "fill",
      "interactive": true,
      "source": "honitby",
      "source-layer": "honitby_stavy13",
      "filter": [">", zvire, breaks[zvire][4]],
      "paint": {
        "fill-color": "#d7191c",
        "fill-outline-color": "#d9d9d9",
        "fill-opacity": 0.8
    }
    }];
    return style;
};


var drawChart = function(honitba, zvire) {
		$("#chart").empty()
        query = "query?where=honitba+%3D+%27" + honitba + "%27+AND+zvire+%3D+%27" + zvire + "%27&outFields=*&returnGeometry=false&f=pjson";
        d3.json(table + query, function(json) {
            data = []
            attribs = json.features[0].attributes

            for (rok in attribs){
                if (rok.slice(0, 1) == "r") {
                    data.push(attribs[rok] | 0) //pozor, chybějící hodnoty nahrazeny nulou!
                } else {
                    continue
                };
            };

            var w = 210;
            var h = 80;

            var yscale = d3.scale.linear()
                            .domain([0, d3.max(data)])
                            .range([0, h])

            var svg = d3.select("#chart")
                .append("svg")
                .attr("width", w)
                .attr("height", h);
            
            svg.selectAll("rect")
               .data(data)
               .enter()
               .append("rect")
               .attr("x", function(d,i) {
                return i * 16;
               })
               .attr("y", function(d) {
                return h - yscale(d)
               })
               .attr("width", 14)
               .attr("height", function(d) {
                return yscale(d);
               })
               .attr("fill", "#66c2a5")


            svg.selectAll("text")
                .data(data)
                .enter()
                .append("text")
                .text(function(d) {
                    return d;
                })
                .attr("x", function(d, i) {
                    return i * 16;
                })
                .attr("y", function(d) {
                    if (d != 0) {
                        return h - yscale(d) + 9;
                    } else {
                        return h - yscale(d)
                    }
                    
                })
                .attr("font-family", "sans-serif")
                .attr("font-size", "10px")
                .attr("fill", "#4d4d4d")
        });
    };

var drawMap = function(zvire) {
	$("#map").empty()
	$("#chart").empty()
	map = new mapboxgl.Map({
	  container: 'map',
	  center: [14.46562, 50.05981],
	  zoom: 8,
	  style: style(zvire, breaks)
	});

	map.dragRotate.disable();
	//map.addControl(new mapboxgl.Navigation());

	map.on('mousemove', function(e) {
	    map.featuresAt(e.point, {}, function(err, features) {
	        if (err) throw err;
	        $(".info").empty()
	        $("#chart").empty()
	        $(".info").append("Honitba " + features[0].properties['NAZEV'] + "<br>" + zverNazvy[zvire.replace('JKS_', '').replace('_STAV', '').replace('_pct', '')] + ": " + features[0].properties[zvire.substr(0, zvire.length - 4)])
	    });
	});

	map.on('click', function(e) {
	    map.featuresAt(e.point, {}, function(err, features) {
	        if (err) {throw err};
	        drawChart(features[0].properties.HONITBA, zvire.replace('_pct', ''))
	    });
	});
};

drawMap("JKS_JELENISA_pct");

$(".selector").change(function(evt) {
    drawMap(evt.target.value)
});